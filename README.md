**Allan Gray Twitter Simulator**

The allan_gray_assignment project is a coding assessment for Lecton Ramasila (lectonlm@gmail.com).

The allan_gray_assignment application, simulates the Twitter social medial platform.

The application requires list of users and tweets as inputs.

It maps the provided users to the provided tweets.

Once the mapping is complete, it prints the map tree in the format define in the 'Retail IT Coding Assignment Version 5.pdf' document, this document
can be found here: src/main/resource/Retail IT Coding Assignment Version 5.pdf

**Environment**

The following information is for the environment setup you should use for testing and running the appliction.

Docker Image Name: python-api
Docker Registry: cibds.azurecr.io
Application Port: 1305
Forwarded Port: 1305

|    Item   | Name |   Version   |
|------|------|------|
| Language | Java | ```1.8.0_201``` |
| Build Tool | Gradle | ```5.4.1``` |
| JVM | JVM | ``` 1.8.0_201``` |

**Run Application**

To run the application, please follow the steps below:

* Place your input files in the following location: ```src/main/resource``` folder. Please enure to place **BOTH THE LIST OF USERS AND TWEETS TXT FILES** in this folder. Remember the names of these input files as we are going to use them to run the application later.
* Navigate to the parent directory, namely, the ```allan_grey_assignment```. Make sure you can see the **build.gradle** file and **src** folder.
* Building the Project
    * To build the project, run the following command: ```gradle clean build```. See the diagram below.
    ![](images/build.png)
* Run Intergration Tests
    * To run the interfration tests, run the following command: ```gradle clean test```
    * I have configured the grable project to print out the **test task** in a format the just shows the success/failure result of the test.
    * Please look at the diagram above.
* Run the Application
    * To run the application, please run the following command, with the appropriate inputs. An example is also provided in the diagram below:
        * Command: ```gradle run --args="USERS_FILE TWEETS_FILE"```
        * USERS_FILE : This is the .txt file with all the users and their relationships, in the same format/schema as the provided example ```user.txt``` file that can be found in ```src/main/resources```.
        * TWEETS_FILE : This is the .txt file with all the tweets and their owners, in the same format/schema as the provided example ```tweet.txt``` file that can be found in ```src/main/resources```.
        * ![](images/run.png)
    * **NB**
        * Please name sure that the file with the Users comes before the file with the tweets.
        * Please provide the input files.
        * Please ensure that all the users with a tweet (.i.e. users in the ```TWEETS_FILE```) actually exist in the ```TWEETS_FILE``` file.
        

**Assumptions**

Provided below is a list of all te assumptions.

* The enviroment is set-up as instructed above.
* The User input file is in a txt file, with the word "follows" as a delimeter.
* The User input file is provided first in the input string.
* The User provided file is places in ```src/main/resources``` folder.
* The Tweet input file is in a txt file, with the word ">" as a delimeter.
* The Tweet input file is provided first in the input string.
* The Tweet provided file is places in ```src/main/resources``` folder.
* All the users in the Tweet input file exist in the User input file.
