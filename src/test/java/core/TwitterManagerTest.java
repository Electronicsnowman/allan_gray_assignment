package core;


import io.IOTweets;
import modules.User;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TwitterManagerTest {
    static TwitterManager twitterManager = null;
    static String inputFileNameUsers;
    static String inputFileNameTweets;


    @Test
    void test_add_single_user_to_the_data_mapper_structure() throws URISyntaxException, IOException {

        twitterManager.put(new User("user1"), new User("user1"));
        int expected = 1;
        int actual = twitterManager.getUserSize();
        assertEquals(expected ,actual);
    }

    @Test
    void test_add_two_users_to_the_data_mapper_structure() throws URISyntaxException, IOException {

        twitterManager.put(new User("user1"), new User("user2"));
        int expected = 2;
        int actual = twitterManager.getUserSize();
        assertEquals(expected ,actual);
    }

    @Test
    void test_user_tweet_mapping() throws URISyntaxException, IOException {

        User user = new User("user1");
        twitterManager.put(user, user);
        twitterManager.assign(
                user,
                "Test tweet 1"
        );

        int expected = 1;
        int actual = twitterManager.getUserTwitterSize(user);
        assertEquals(expected ,actual);
    }

    @Test
    void test_user_tweet_mapping_for_multiple_tweets() throws URISyntaxException, IOException {

        User user = new User("user");
        twitterManager.put(user, user);
        twitterManager.assign(
                user,
                "Test tweet 1"
        );
        twitterManager.assign(
                user,
                "Test tweet 2"
        );
        twitterManager.assign(
                user,
                "Test tweet 3"
        );
        twitterManager.assign(
                user,
                "Test tweet 4"
        );

        int expected = 4;
        int actual = twitterManager.getUserTwitterSize(user);
        assertEquals(expected ,actual);
    }

    @BeforeAll
    public static void setUp() {
        twitterManager = new TwitterManager();
        inputFileNameTweets = "tweet.txt";
        inputFileNameUsers = "user.txt";
    }

    @AfterAll
    private static void tearDown(){

        twitterManager = null;
    }
}
