package io;

import core.TwitterManager;
import modules.User;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IOUsersTest{

    static String inputFileName;
    static TwitterManager twitterManager;
    static IOUsers iOUsers;

    @Test
    void test_that_the_users_input_file_exits() throws URISyntaxException {
        File file = new File(ClassLoader.getSystemClassLoader().getResource(inputFileName).toURI());
        boolean expected = true;
        assertEquals(expected ,file.exists());

    }

    @Test
    void test_that_the_users_input_file_type_is_txt() throws URISyntaxException {
        File file = new File(ClassLoader.getSystemClassLoader().getResource(inputFileName).toURI());
        String expected = "txt";
        String actual = inputFileName.substring(file.getName().lastIndexOf('.') + 1);
        assertEquals(expected ,actual);

    }

    @Test
    void test_that_the_users_input_file_read_method() throws URISyntaxException, IOException {
        boolean expected = true;
        boolean actual = iOUsers.read(ClassLoader.getSystemClassLoader().getResource(inputFileName).toURI() ,twitterManager);
        assertEquals(expected ,actual);
    }

    @BeforeAll
    public static void setUp() {
        inputFileName = "user.txt";
        iOUsers = new IOUsers();
        twitterManager = new TwitterManager();
    }

    @AfterAll
    private static void tearDown(){
        iOUsers = null;
    }
}