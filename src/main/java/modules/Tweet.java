/**
 * The User class encapsulates a tweet object as well as the user who owns the tweet.
 * The tweet is assigned a Id by the TweeterManager, that is determined by the order of insertion.
 * The class also implements the comparable interface, this is so that we can define how tweets should be compared to each other,
 * in this case, tweets care compared by their Id.
 *
 * @author Lecton Ramasila
 * @version 1.0
 */


package modules;

public class Tweet implements Comparable<Tweet>{
    private int Id;
    private String author;
    private String message;

    public Tweet(int id, String author, String message) {
        Id = id;
        this.author = author;
        this.message = message;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String toString(){
        String out = "";
            out += "@" + this.author + ":" + this.message;
        return out;
    }

    @Override
    public int compareTo(Tweet tweet) {
        return this.Id - tweet.Id;
    }
}
