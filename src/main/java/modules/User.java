/**
 * The User class encapsulates a user object as well as the users respective tweets.
 * The users tweets are stored in a list, representing all the users tweets.
 * The class also implements the comparable interface, this is so that we can define how users should be compared to each other,
 * in this case, users care compared by their names.
 *
 * @author Lecton Ramasila
 * @version 1.0
 */

package modules;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class User implements Comparable<User>{

    private String name;
    private List<Tweet> tweets;

    public User(String name) {
        this.name = name;
        this.tweets = new ArrayList<>();
    }

    public User(String name, List<Tweet> following) {
        this.name = name;
        this.tweets = following;
    }

    public String getName() {
        return name;
    }

    public List<Tweet> getTweets() {
        return tweets;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addTweet(Tweet tweet) {

        this.tweets.add(tweet);
    }

    public int hashCode(){
        return this.name.hashCode();
    }

    public boolean equals(Object obj){
        if (obj instanceof User) {
            User user = (User) obj;
            return (user.name.equals(this.name));
        } else {
            return false;
        }
    }

    public String toString(){
        String out = "";
        String space = "\n\t\t";

        out = this.name +  space;
        for(Tweet tweet: tweets){
            out += tweet + space;
        }
        return out;
    }

    @Override
    public int compareTo(User user) {
        return this.name.compareTo(user.name);
    }
}
