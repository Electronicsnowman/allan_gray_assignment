/**
* The TwitterManager class performs the mapping logic between a series of users, followers and their respective tweets.
* The class makes use of a Map data structure to capture the relationship between users and followers.
* Each User stores all of its tweets, each tweet has a 'auto-increment' id that is determined by order of insert.
*
* @author Lecton Ramasila
* @version 1.0
 */
package core;

import modules.Tweet;
import modules.User;
import java.util.*;

public class TwitterManager{
        private Map<User, Set<User>> users = new HashMap<>();
        int tweetId = 1;

        public void put(User first, User second) {
            if (!users.containsKey(first)) {
                users.put(first, new HashSet<>());
            }
            users.get(first).add(second);

            if (!users.containsKey(second)) {
                users.put(second, new HashSet<>());
            }
            users = new TreeMap<>(users);
        }

        public int getUserSize(){
            return users.size();
        }


        public int getUserTwitterSize(User u){
            return getEntryByKey(u).getKey().getTweets().size();
        }
        public void assign(User u, String tweet){
            Map.Entry<User, Set<User>> record = getEntryByKey(u);

            if(null != record){
                User a = (User)record.getKey();
                a.addTweet(new Tweet(
                        tweetId,
                        a.getName(),
                        tweet
                ));
                ++tweetId;

            }
        }

        public Map.Entry<User, Set<User>> getEntryByKey(User key){
            Map.Entry<User, Set<User>> record = null;
            for (Map.Entry<User, Set<User>> user : users.entrySet()) {

                if(key.equals(user.getKey())){
                    return user;
                }
            }
            return null;
        }


        private List<Tweet> combine(User user, Set<User> followers){

            List<Tweet> tweets = user.getTweets();

            for (User follower: followers) {
                tweets.addAll(follower.getTweets());
            }
            Collections.sort(tweets);
            return tweets;
        }

        public String toString(){
            String out = "";
            String space = "\n";

            for (Map.Entry<User, Set<User>> user : users.entrySet()) {

                out += user.getKey().getName() + "\n\t";
                List<Tweet> tweets = combine(user.getKey() ,user.getValue());

                for (Tweet tweet: tweets) {
                    out += tweet+ "\n\t";
                }
                out = out.substring(0, out.length() - 2) + "\n";
            }

            return out;
        }
}
