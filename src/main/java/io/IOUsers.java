/**
 * The IOUsers class reader user information from a given text file and adds them to a TwitterManager object that is passed to the 'read' function.
 * The 'read' function in the class, is a implementation of the function signature defined in the 'IOInterface' interface.
 *
 * @author Lecton Ramasila
 * @version 1.0
 */
package io;

import core.TwitterManager;
import modules.User;
import java.io.*;
import java.net.URI;

public class IOUsers implements IOInterface {

    public IOUsers(){}

    public boolean read(URI path, TwitterManager tm) throws FileNotFoundException, IOException {
        File file = new File(path);

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String delimete = "follows";
            String line;
            String [] segments;
            String [] followings;
            String userName = "";
            String userFollower = "";
            while (null != (line = br.readLine())) {
                segments = line.split(delimete);
                followings = segments[1].trim().split(",");
                for(String following : followings) {
                    tm.put(new User(segments[0].trim()), new User(following.trim()));
                }
            }
        }catch (FileNotFoundException e) {
            throw new FileNotFoundException(e.getMessage());
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
        return true;
    }
}
