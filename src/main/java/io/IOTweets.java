/**
 * The IOTweets class reader user information from a given text file and adds them to a TwitterManager object that is passed to the 'read' function.
 * It passes a tweet, and owner of the tweet in the form of a User object to the TwitterManager object, so that it can perform the appropriate mapping.
 * The 'read' function in the class, is a implementation of the function signature defined in the 'IOInterface' interface.
 *
 * @author Lecton Ramasila
 * @version 1.0
 */
package io;

import core.TwitterManager;
import modules.User;
import java.io.*;
import java.net.URI;

public class IOTweets implements IOInterface {

    public IOTweets(){}

    public boolean read(URI path, TwitterManager tm) throws FileNotFoundException, IOException {
        File file = new File(path);

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String delimete = ">";
            String line;
            String [] segments;
            while (null != (line = br.readLine())) {
                segments = line.split(delimete);
                tm.assign(
                        new User(segments[0].trim()),
                        segments[1]
                );
            }
        }catch (FileNotFoundException e) {
            throw new FileNotFoundException(e.getMessage());
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
        return true;
    }
}
