package io;

import core.TwitterManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;

public interface IOInterface {
    public boolean read(URI path , TwitterManager tm) throws FileNotFoundException, IOException;
}
