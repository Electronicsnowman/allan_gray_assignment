/**
 * The allan_gray_assignment application, simulates the Twitter social medial platform.
 * The application requires list of users and tweets as inputs.
 * It maps the provided users to the provided tweets
 * Once the mapping is complete, it prints the map tree in the format define in the 'Retail IT Coding Assignment Version 5.pdf' document, this document
 * can be found here: src/main/resource/Retail IT Coding Assignment Version 5.pdf
 * @author Lecton Ramasila
 * @version 1.0
 */

import core.TwitterManager;
import io.IOTweets;
import io.IOUsers;
import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;

public class Main extends IOUsers {

    private static TwitterManager twitterManager = new TwitterManager();

    public static void main(String [] args){
        try {

            URL usersFileURL = ClassLoader.getSystemClassLoader().getResource(args[0]);
            URL tweetsFileURL = ClassLoader.getSystemClassLoader().getResource(args[1]);
            new IOUsers().read(usersFileURL.toURI() ,twitterManager);
            new IOTweets().read(tweetsFileURL.toURI() ,twitterManager);

        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        System.out.println(twitterManager);
    }

}
